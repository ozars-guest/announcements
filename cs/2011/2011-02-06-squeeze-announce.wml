<define-tag pagetitle>Debian 6.0 <q>Squeeze</q> uvoln�n</define-tag>
<define-tag release_date>2011-02-05</define-tag>
#use wml::debian::translation-check translation="1.1" maintainer="Miroslav Kure"
#use wml::debian::news

<p>Projekt Debian s pot�en�m oznamuje, �e po 24 m�s�c�ch
nep�etr�it�ho v�voje byl uvoln�n Debian verze 6.0, k�dov�m ozna�en�m
<q>Squeeze</q>. Debian je svobodn� opera�n� syst�m, kter� poprv�
p�ich�z� ve dvou variant�ch. Spole�n� s Debian GNU/Linuxem je vyd�no
i <q>technologick� p�edstaven�</q> Debian GNU/kFreeBSD.</p>

<p>Debian 6.0 obsahuje KDE Plasma Desktop v�etn� aplikac�, desktopov�
prost�ed� GNOME, Xfce a LXDE a tak� spoustu serverov�ch aplikac�. Je
kompatibiln� s FHS v2.3 a podporuje software vyvinut� pro LSB 3.2.</p>

<p>Debian b�� na nejr�zn�j��ch po��ta��ch od palmtop� a handheld� a�
po superpo��ta�e a t�m�� �emkoliv mezi t�m. Celkov� je podporov�no
dev�t architektur zahrnuj�c�ch 32 bitov� PC / Intel IA-32
(<code>i386</code>), 64 bitov� PC / Intel EM64T / x86-64
(<code>amd64</code>), Motorola/IBM PowerPC (<code>powerpc</code>),
Sun/Oracle SPARC (<code>sparc</code>), MIPS (<code>mips</code>
(big-endian), <code>mipsel</code> (little-endian)), Intel Itanium
(<code>ia64</code>), IBM S/390 (<code>s390</code>) a ARM EABI
(<code>armel</code>).</p>

<p>Debian 6.0 <q>Squeeze</q> p�edstavuje dva nov� porty u�ivatelsk�ho
prost�ed� Debian/GNU na j�dro z projektu FreeBSD: Debian GNU/kFreeBSD
pro 32 bitov� (<code>kfreebsd-i386</code>) a 64 bitov�
(<code>kfreebsd-amd64</code>) PC. Jedn� se o �pln� prvn� porty Debianu
na jin� j�dro ne� Linux, kter� se staly sou��st� ofici�ln�ho vyd�n�.
Z pohledu b�n�ho serverov�ho software je podpora siln�j�� ne� kdy
d��ve, proto�e kombinuje st�vaj�c� vlastnosti linuxov�ho Debianu
s unik�tn�mi vlastnostmi ze sv�ta BSD. V tomto prvn�m vyd�n� jsou v�ak
oba nov� porty pon�kud omezeny, nap��klad zat�m nejsou podporov�ny
n�kter� pokro�il� vlastnosti desktopov�ch prost�ed�.</p>

<p>Dal��m prvenstv�m je kompletn� svobodn� linuxov� j�dro, kter� ji�
neobsahuje ��dn� problematick� firmware. Ten byl odd�len do
samostatn�ch bal�k� a p�esunut z Debianu do ��sti non-free, kter� nen�
standardn� povolen�. Takto mohou u�ivatel� Debianu pou��vat zcela
svobodn� opera�n� syst�m, ale v p��pad� nutnosti se mohou rozhodnout
i pro nesvobodn� firmware. Pot�ebn� nesvobodn� firmware m��e b�t
nahr�n b�hem instalace, dostupn� jsou tak� speci�ln� CD m�dia
a soubory pro instalaci z USB. V�ce informac� naleznete na wiki
str�nce <a href="http://wiki.debian.org/Firmware">Debian
a firmware</a>.</p>

<p>Debian 6.0 d�le p�in�� syst�m zav�d�n� zalo�en� na z�vislostech.
To jednak zrychluje start syst�mu d�ky paraleln�mu spou�t�n�
zav�d�c�ch skript� a tak� zvy�uje jeho robustnost d�sledn�m sledov�n�m
z�vislost� mezi nimi. R�zn� dal�� zm�ny zlep�uj� pou�it� na men��ch
notebooc�ch, nam�tkou za�azen� kab�tku KDE Plasma Netbook.</p>

<p>Toto vyd�n� obsahuje mno�stv� aktu�ln�ch softwarov�ch bal�k�
jako:</p>

<ul>
<li>KDE Plasma Workspaces a KDE Applications 4.4.5</li>
<li>aktualizovan� desktopov� prost�ed� GNOME 2.30</li>
<li>desktopov� prost�ed� Xfce 4.6</li>
<li>LXDE 0.5.0</li>
<li>X.Org 7.5</li>
<li>OpenOffice.org 3.2.1</li>
<li>GIMP 2.6.11</li>
<li>Iceweasel 3.5.16 (p�ejmenovan� Mozilla Firefox)</li>
<li>Icedove 3.0.11 (p�ejmenovan� Mozilla Thunderbird)</li>
<li>PostgreSQL 8.4.6</li>
<li>MySQL 5.1.49</li>
<li>GNU Compiler Collection 4.4.5</li>
<li>Linux 2.6.32</li>
<li>Apache 2.2.16</li>
<li>Samba 3.5.6</li>
<li>Python 2.6.6, 2.5.5 a 3.1.3</li>
<li>Perl 5.10.1</li>
<li>PHP 5.3.3</li>
<li>Asterisk 1.6.2.9</li>
<li>Nagios 3.2.3</li>
<li>Xen Hypervisor 4.0.1 (podpora pro dom0 i domU)</li>
<li>OpenJDK 6b18</li>
<li>Tomcat 6.0.18</li>
<li>a v�ce ne� 29�000 dal��ch softwarov�ch bal�k� p�ipraven�ch k
okam�it�mu pou�it�, sestaven�ch z t�m�� 15�000 zdrojov�ch bal�k�.</li>
</ul>

<p>Debian 6.0 obsahuje na 10�000 nov�ch bal�k� jako t�eba prohl��e�
Chromium, monitorovac� �e�en� Icinga, rozhran� pro spr�vu bal�k�
Software Center, spr�vce s��ov�ch p�ipojen� wicd, n�stroje pro Linux
Containers lxc nebo klastrovac� framework Corosync.</p>

<p>S t�mto �irok�m v�b�rem softwaru Debian op�t potvrzuje sv�j z�m�r
b�t univerz�ln�m opera�n�m syst�mem. Je vhodn� pro mnoho odli�n�ch
zp�sob� nasazen�: od desktop� k netbook�m, od v�voj��sk�ch server� ke
klastr�m a produk�n�m datab�zov�m, webov�m �i datov�m server�m. Aby
Debian 6.0 splnil vysok� o�ek�v�n�, kter� u�ivatel� stabiln�ho vyd�n�
p�edpokl�daj�, byly pou�ity dodate�n� zp�soby zaji�t�n� kvality jako
nap��klad automatick� testov�n� instalac� a aktualizac� v�ech bal�k�
v archivu.</p>

<p>Po��naje Debianem 6.0 jsou specializovan� distribuce <q>Custom
Debian Distributions</q> p�ejmenov�ny na <a
href="http://blends.alioth.debian.org/"><q>Debian Pure
Blends</q></a>. Jejich z�b�r se v�razn� roz���il, kdy� ke st�vaj�c�m
<q>pure blends</q>
<a href="http://wiki.debian.org/DebianEdu">Debian Edu</a>,
<a href="http://www.debian.org/devel/debian-med/">Debian Med</a> a
<a href="http://wiki.debian.org/DebianScience">Debian Science</a>
p�ibyly z�jmov� skupiny
<a href="http://www.debian.org/devel/debian-accessibility/">Debian
Accessibility</a>,
<a href="http://debichem.alioth.debian.org/">DebiChem</a>,
<a href="http://wiki.debian.org/DebianEzGo">Debian EzGo</a>,
<a href="http://wiki.debian.org/DebianGis">Debian GIS</a> a
<a href="http://blends.alioth.debian.org/multimedia/tasks/index">Debian
Multimedia</a>.
Kompletn� obsah v�ech <q>pure blends</q> lze <a
href="http://blends.alioth.debian.org/">proch�zet online</a>, kde mohou
u�ivatel� tak� nominovat bal�ky pro za�azen� do p���t�ho vyd�n�.</p>

<p>Debian lze instalovat z nejr�zn�j��ch m�di�, jako jsou Blu-ray,
DVD, CD, USB pam�ti, nebo t�eba s��. V�choz�m desktopov�m prost�ed�m
je GNOME a nach�z� se na prvn�m CD. Ostatn� desktopov� prost�ed� (KDE
Plasma Desktop, Xfce a LXDE) mohou b�t instalov�na pomoc� dvou
alternativn�ch obraz� CD. Zvolen� prost�ed� lze vybrat z nab�dky
rovnou p�i zav�d�n� instala�n�ho syst�mu. Debian 6.0 op�t podporuje
v�cearchitekturn� CD a DVD, kter� umo��uj� instalaci na v�ce
architektur�ch z jedin�ho m�dia. Zna�n� zjednodu�eno bylo vytv��en�
instala�n�ch USB m�di�, v�ce se dozv�te v <a
href="$(HOME)/releases/squeeze/installmanual">instala�n�
p��ru�ce</a>.</p>

<p>K dispozici jsou tak� speci�ln� obrazy, d�ky kter�m lze Debian
GNU/Linux pou��vat p��mo, bez p�edchoz� instalace. Tyto speci�ln�
obrazy se naz�vaj� <q>�iv�</q> (live) obrazy a jsou dostupn� pro CD,
USB pam�ti a r�zn� varianty zav�d�n� ze s�t�. Zat�m jsou tyto obrazy
dostupn� pouze na architektur�ch <code>amd64</code>
a <code>i386</code> a je mo�n� je pou��t i pro instalaci Debian
GNU/Linuxu.</p>

<p>Instala�n� proces Debian GNU/Linuxu 6.0 byl v mnoha sm�rech
vylep�en. Krom� jin�ho se zjednodu�il v�b�r jazyka a kl�vesnice nebo
rozd�len� disku p�i pou�it� LVM, RAIDu nebo �ifrov�n�. Nov� jsou
podporov�ny souborov� syst�my ext4 a Btrfs a na architektu�e kFreeBSD
ZFS (Zettabyte filesystem). Instala�n� syst�m Debianu je p�elo�en do
70 jazyk�.</p>

<p>Debian GNU/Linux si ji� nyn� m��ete st�hnout z Internetu p�es
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (doporu�en� mo�nost),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> nebo
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; vizte
<a href="$(HOME)/CD/">Debian na CD</a>. V brzk� dob� bude nov�
Debian dostupn� i u <a href="$(HOME)/CD/vendors">prodejc�</a> na DVD,
CD a Blu-ray disc�ch.</p>

<p>P�echod z p�edchoz� verze Debianu 5.0 <q>Lenny</q> je ve v�t�in�
p��pad� �e�en automaticky n�strojem pro spr�vu bal�k� apt-get a do
jist� m�ry t� programem aptitude. Aktualizace Debianu m��e b�t jako
v�dy provedena bezbolestn�, na m�st� a bez zbyte�n� odst�vky syst�mu,
ov�em d�razn� doporu�ujeme p�e��st si <a
href="$(HOME)/releases/squeeze/releasenotes">pozn�mky k vyd�n�</a>
a <a href="$(HOME)/releases/squeeze/installmanual">instala�n�
p��ru�ku</a> a p�edej�t tak potenci�ln�m probl�m�m. Poznamenejme, �e
v nadch�zej�c�ch t�dnech se budou pozn�mky d�le vylep�ovat a budou
dostupn� ve v�ce jazyc�ch.</p>


<h2>O Debianu</h2>

<p>Debian je svobodn� opera�n� syst�m vyv�jen� tis�covkami
dobrovoln�k� z cel�ho sv�ta spolupracuj�c�ch prost�ednictv�m
Internetu. Hlavn�mi siln�mi str�nkami projektu Debian jsou
dobrovolnick� z�kladna, dodr�ov�n� spole�ensk� smlouvy Debianu (Debian
Social Contract) a odhodl�n� poskytovat nejlep�� mo�n� opera�n�
syst�m. Debian 6.0 je v tomto sm�ru dal��m d�le�it�m krokem.</p>


<h2>Kontaktn� informace</h2>

<p>Pro dal�� informace pros�m nav�tivte webov� str�nky Debianu na
<a href="$(HOME)/">http://www.debian.org/</a> nebo za�lete email na
adresu &lt;press@debian.org&gt;.</p>
